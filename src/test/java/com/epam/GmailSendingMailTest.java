package com.epam;

import com.epam.bo.GmailLogInBO;
import com.epam.bo.MessageSendingBO;
import com.epam.utils.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GmailSendingMailTest {
    private static final String BASE_PAGE = "https://mail.google.com/";
    private static final String USER_EMAIL = "jurageryak@gmail.com";
    private static final String USER_PASSWORD = "********";
    private static Logger logger = LogManager.getLogger();
    private WebDriver driver;
    private GmailLogInBO logInBO;
    private MessageSendingBO sendingBO;

    @BeforeClass
    private void setUp() {
        logger.info("initializing driver");
        driver = WebDriverManager.getDriver();
        logger.info("loading Gmail LogIn page");
        driver.get(BASE_PAGE);
        logInBO = new GmailLogInBO(driver);
        sendingBO = new MessageSendingBO(driver);
    }

    @DataProvider
    private Object[][] buildMessage() {
        return new Object[][]{{"yuraheriak@ukr.net", "Selenium Message", "Hello from Selenium WebDriver!"}};
    }

    @Test
    private void testLoggingInGmail() {
        logger.info("logging in Gmail account");
        logInBO.login(USER_EMAIL, USER_PASSWORD);
        logger.info("waiting for loading user home page");
        new WebDriverWait(driver, 40).until(dr -> dr.getTitle().contains(USER_EMAIL));
        logger.info("user page was successfully loaded");

    }

    @Test(dataProvider = "buildMessage")
    private void testSendingMessage(String receiver, String subject, String message) {
        new WebDriverWait(driver, 40).until(ExpectedConditions
                .elementToBeClickable(By.xpath("//div[@class='aic']//div[@role='button']")));
        logger.info("test sending message");
        sendingBO.sendMessage(receiver, subject, message);
        WebElement viewMessageLink = new WebDriverWait(driver, 20).until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//*[@id='link_vsm']")));
        viewMessageLink.click();
        new WebDriverWait(driver, 20).until(dr -> dr.getTitle().contains(subject + " - " + USER_EMAIL));
        logger.info("message was successfully sent");
    }

    @AfterClass
    private void tearDown() {
        logger.info("closing driver");
        WebDriverManager.closeDriver();
    }
}
