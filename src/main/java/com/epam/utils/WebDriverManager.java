package com.epam.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class WebDriverManager {
    private static final String DRIVER_PATH = "src/main/resources/chromedriver.exe";
    private static final String DRIVER_NAME = "webdriver.chrome.driver";
    private WebDriver driver;
    private static WebDriverManager manager;

    private WebDriverManager() {
        if (Objects.nonNull(manager)) {
            throw new IllegalStateException();
        } else {
            initDriver();
        }
    }

    public static WebDriver getDriver() {
        if (Objects.isNull(manager)) {
            manager = new WebDriverManager();
        }
        return manager.driver;
    }

    public static WebDriverWait getDriverWait(long time) {
        return new WebDriverWait(getDriver(), time);
    }

    private void initDriver() {
        System.setProperty(DRIVER_NAME, DRIVER_PATH);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=uk");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().fullscreen();
    }

    public static void closeDriver() {
        manager.driver.quit();
        manager.driver = null;
    }
}
