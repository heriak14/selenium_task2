package com.epam.bo;

import com.epam.po.IncomingMessagesPage;
import org.openqa.selenium.WebDriver;

public class MessageSendingBO {
    private IncomingMessagesPage messagesPage;

    public MessageSendingBO(WebDriver driver) {
        messagesPage = new IncomingMessagesPage(driver);
    }

    public void sendMessage(String receiverEmail, String subject, String message) {
        messagesPage.clickComposeButton();
        messagesPage.enterReceiverEmail(receiverEmail);
        messagesPage.enterSubject(subject);
        messagesPage.enterMessage(message);
        messagesPage.clickSendButton();
    }
}
