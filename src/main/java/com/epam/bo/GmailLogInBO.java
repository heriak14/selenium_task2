package com.epam.bo;

import com.epam.po.EmailInputPage;
import com.epam.po.PasswordInputPage;
import org.openqa.selenium.WebDriver;

public class GmailLogInBO {
    private EmailInputPage emailPage;
    private PasswordInputPage passwordPage;

    public GmailLogInBO(WebDriver driver) {
        emailPage = new EmailInputPage(driver);
        passwordPage = new PasswordInputPage(driver);
    }

    public void login(String email, String password) {
        emailPage.enterEmail(email);
        emailPage.clickNextButton();
        passwordPage.enterPassword(password);
        passwordPage.clickNextButton();
    }
}
