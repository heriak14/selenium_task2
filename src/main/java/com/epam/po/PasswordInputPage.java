package com.epam.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PasswordInputPage {
    @FindBy(css = "#password div.aCsJod.oJeWuf div div.Xb9hP input")
    private WebElement passwordField;
    @FindBy(id = "passwordNext")
    private WebElement nextButton;

    public PasswordInputPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void enterPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickNextButton() {
        nextButton.click();
    }
}
