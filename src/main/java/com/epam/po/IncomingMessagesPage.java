package com.epam.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IncomingMessagesPage {
    @FindBy(css = "div.aic div[role='button']")
    private WebElement composeButton;
    @FindBy(css = "textarea[name='to']")
    private WebElement receiverField;
    @FindBy(css = "input[name='subjectbox']")
    private WebElement subjectField;
    @FindBy(xpath = "//div[@class='Ar Au']//div[@role='textbox']")
    private WebElement messageField;
    @FindBy(css = "div.dC > div[role='button']")
    private WebElement sendButton;

    public IncomingMessagesPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void clickComposeButton() {
        composeButton.click();
    }

    public void enterReceiverEmail(String email) {
        receiverField.sendKeys(email);
    }

    public void enterSubject(String subject) {
        subjectField.sendKeys(subject);
    }

    public void enterMessage(String message) {
        messageField.sendKeys(message);
    }

    public void clickSendButton() {
        sendButton.click();
    }
}
