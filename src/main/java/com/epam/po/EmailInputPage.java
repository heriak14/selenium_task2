package com.epam.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmailInputPage {
    @FindBy(id = "identifierId")
    private WebElement emailField;
    @FindBy(id = "identifierNext")
    private WebElement nextButton;

    public EmailInputPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void enterEmail(String email) {
        emailField.sendKeys(email);
    }

    public void clickNextButton() {
        nextButton.click();
    }
}
